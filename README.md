## full_oppo6771_18611-user 8.1.0 O11019 1556007842 release-keys
- Manufacturer: realme
- Platform: mt6771
- Codename: RMX1831
- Brand: Realme
- Flavor: full_oppo6771_18611-user
- Release Version: 8.1.0
- Id: O11019
- Incremental: 1556008001
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: 480
- Fingerprint: 
- OTA version: 
- Branch: full_oppo6771_18611-user-8.1.0-O11019-1556007842-release-keys
- Repo: realme_rmx1831_dump_4563


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
