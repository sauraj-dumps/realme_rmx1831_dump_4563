#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/bootdevice/by-name/recovery:42841712:1a7a176345424cbcbb0139bc18f2e960427ef8e7; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/platform/bootdevice/by-name/boot:11525744:6850d7e5c4137c69429a008487916161b0906e9d EMMC:/dev/block/platform/bootdevice/by-name/recovery 1a7a176345424cbcbb0139bc18f2e960427ef8e7 42841712 6850d7e5c4137c69429a008487916161b0906e9d:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
